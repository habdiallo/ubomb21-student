/*
 * Copyright (c) 2020. Laurent Réveillère
 */

package fr.ubx.poo.ubomb.engine;

import fr.ubx.poo.ubomb.game.Direction;
import fr.ubx.poo.ubomb.game.Game;
import fr.ubx.poo.ubomb.game.Position;
import fr.ubx.poo.ubomb.go.character.Character;
import fr.ubx.poo.ubomb.go.character.Monster;
import fr.ubx.poo.ubomb.go.character.Player;
import fr.ubx.poo.ubomb.go.decor.Decor;
import fr.ubx.poo.ubomb.go.decor.bombs.EBombs;
import fr.ubx.poo.ubomb.go.decor.bombs.Explosion;
import fr.ubx.poo.ubomb.view.*;
import javafx.animation.AnimationTimer;
import javafx.application.Platform;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

import java.util.*;


public final class GameEngine {

    private static AnimationTimer gameLoop;
    private final String windowTitle;
    private final Game game;
    private final Player player;
    private final List<Sprite> sprites = new LinkedList<>();
    private final Set<Sprite> cleanUpSprites = new HashSet<>();

    private final List<Explosion> explosionList = new ArrayList<>();

    private final Stage stage;
    private StatusBar statusBar;
    private Pane layer;
    private Input input;


    private Explosion exp = null;

    public GameEngine(final String windowTitle, Game game, final Stage stage) {
        this.stage = stage;
        this.windowTitle = windowTitle;
        this.game = game;
        this.player = game.getPlayer();
        initialize();
        buildAndSetGameLoop();
    }

    private void initialize() {
        Group root = new Group();
        layer = new Pane();

        int height = game.getGrid().getHeight();
        int width = game.getGrid().getWidth();
        int sceneWidth = width * Sprite.size;
        int sceneHeight = height * Sprite.size;
        Scene scene = new Scene(root, sceneWidth, sceneHeight + StatusBar.height);
        scene.getStylesheets().add(Objects.requireNonNull(getClass().getResource("/css/application.css")).toExternalForm());

        stage.setTitle(windowTitle);
        stage.setScene(scene);
        stage.setResizable(false);
        stage.sizeToScene();
        stage.show();

        input = new Input(scene);
        root.getChildren().add(layer);
        statusBar = new StatusBar(root, sceneWidth, sceneHeight);

        // Create sprites
        for (Decor decor : game.getGrid().values()) {
            sprites.add(SpriteFactory.create(layer, decor));
            decor.setModified(true);
        }

        // Create spriteMonster
        for (Character monsDec : game.getGrid().characters()) {
            if (monsDec instanceof Monster) {
                sprites.add(new SpriteMonster(layer, monsDec));
                monsDec.setModified(true);
            }
        }

        sprites.add(new SpritePlayer(layer, player));
    }

    void buildAndSetGameLoop() {
        gameLoop = new AnimationTimer() {
            public void handle(long now) {
                // Check keyboard actions
                processInput(now);

                // Do actions
                update(now);

                checkCollision(now);
                checkExplosions(now);

                // Graphic update
                cleanupSprites();

                //Explosion cleanup
                render();
                statusBar.update(game);
            }
        };
    }

    private void checkExplosions(long now) {

        for (EBombs bombs : game.bombList) {
            if (!bombs.isExploded()) {
                int secondMinuter = (int) ((now - bombs.getStartNow()) / 1e9) % 10;
                if (secondMinuter == 1) {
                    bombs.setTimer(2);
                    bombs.setModified(true);
                }
                if (secondMinuter == 2) {
                    bombs.setTimer(1);
                    bombs.setModified(true);
                }
                if (secondMinuter == 3) {
                    bombs.setTimer(0);
                    bombs.setModified(true);
                }
                if (secondMinuter == 4) {
                    bombs.remove();
                    if (exp == null) {
                        exp = new Explosion(game, bombs.getPosition(), now);
                        sprites.add(new SpriteExplosion(layer, ImageResource.EXPLOSION.getImage(), exp));
                        exp.setModified(true);

                        for (Position bPosition : bombs.getExpPosition(now)) {
                            Explosion explosion = new Explosion(game, bPosition, now);

                            //if (explosionList.size() < bombs.getExpPosition(now).size()) {
                                explosionList.add(explosion);
                            //}
                        }
                        for (Explosion expL : explosionList) {
                            sprites.add(new SpriteExplosion(layer, ImageResource.EXPLOSION.getImage(), expL));
                        }
                    }
                    if ((int) ((now - bombs.getStartNow()) / 1e8) % 10 == 1) {
                        for (Explosion explosion : explosionList) {
                            explosion.remove();
                        }
                        exp.remove();
                        explosionList.clear();
                        bombs.setExplodedB(true);
                    }
                }
            }

        }

    }

    private void createNewBombs(long now) {
        if (this.game.getPlayer().getBombNumber() >= 1) {
            EBombs bomb = new EBombs(this.game, player.getPosition(), now);
            sprites.add(new SpriteBombs(layer, bomb));
            this.game.getPlayer().decBombNumber();
            this.game.bombList.add(bomb);
        }
    }

    private void monsterUpdate(long now) {
        for (Character character : game.getGrid().getMonsterGrid()) {
            Monster monster = (Monster) character;
            monster.update(now);
        }
    }

    private void checkCollision(long now) {
        player.checkInvincible(now);
        Position positionPlayer = game.getPlayer().getPosition();
        for (Character character : game.getGrid().getMonsterGrid()) {
            Monster monster = (Monster) character;
            if (monster.getPosition().equals(positionPlayer) && !player.isInvincible()) {
                player.decHeart(now);
            }
        }
    }

    private void processInput(long now) {
        if (input.isExit()) {
            gameLoop.stop();
            Platform.exit();
            System.exit(0);
        } else if (input.isMoveDown()) {
            player.requestMove(Direction.DOWN);
        } else if (input.isMoveLeft()) {
            player.requestMove(Direction.LEFT);
        } else if (input.isMoveRight()) {
            player.requestMove(Direction.RIGHT);
        } else if (input.isMoveUp()) {
            player.requestMove(Direction.UP);
            input.clear();
        } else if (input.isKey()) {
            player.takeDoor();
        } else if (input.isBomb()) {
            createNewBombs(now);
        }
        input.clear();
    }

    private void showMessage(String msg, Color color) {
        Text waitingForKey = new Text(msg);
        waitingForKey.setTextAlignment(TextAlignment.CENTER);
        waitingForKey.setFont(new Font(60));
        waitingForKey.setFill(color);
        StackPane root = new StackPane();
        root.getChildren().add(waitingForKey);
        Scene scene = new Scene(root, 400, 200, Color.WHITE);
        stage.setTitle(windowTitle);
        stage.setScene(scene);
        input = new Input(scene);
        stage.show();
        new AnimationTimer() {
            public void handle(long now) {
                processInput(now);
            }
        }.start();
    }


    private void update(long now) {
        //Player update
        player.update(now);

        if (player.getLives() == 0) {
            gameLoop.stop();
            showMessage("Perdu!", Color.RED);
        }

        if (player.isWinner()) {
            gameLoop.stop();
            showMessage("Gagné", Color.BLUE);
        }
        //Monster update
        monsterUpdate(now);

        //level updating
        if (game.isLevelChanged()) {
            game.setGrid(game.getGridLevel().get(game.getCurrentGameLevel()));
            sprites.clear();
            stage.close();
            initialize();
            game.getPlayer().setPosition(game.doorIsThere());
            game.setLevelChanged(false);
        }
    }

    public void cleanupSprites() {
        sprites.forEach(sprite -> {
            if (sprite.getGameObject().isDeleted()) {
                game.getGrid().remove(sprite.getPosition());
                cleanUpSprites.add(sprite);
            }
        });
        cleanUpSprites.forEach(Sprite::remove);
        sprites.removeAll(cleanUpSprites);
        cleanUpSprites.clear();
    }


    private void render() {
        sprites.forEach(Sprite::render);
    }

    public void start() {
        gameLoop.start();
    }
}
