package fr.ubx.poo.ubomb.view;

import fr.ubx.poo.ubomb.go.GameObject;
import fr.ubx.poo.ubomb.go.decor.bombs.EBombs;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;

public class SpriteBombs extends Sprite{

    public SpriteBombs(Pane layer, GameObject bomb) {
        super(layer, ImageResource.BOMB_3.getImage(), bomb);
        updateImage();
    }

    public void updateImage() {
        EBombs bomb = (EBombs) getGameObject();
        Image image = getImageBomb(bomb.getTimer());
        setImage(image);
    }

    public Image getImageBomb (int bombI){
        return ImageResource.getBomb(bombI);
    }

}
