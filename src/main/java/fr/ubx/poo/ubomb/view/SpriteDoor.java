package fr.ubx.poo.ubomb.view;

import fr.ubx.poo.ubomb.go.GameObject;
import fr.ubx.poo.ubomb.go.decor.doors.Doors;
import javafx.scene.layout.Pane;

import static fr.ubx.poo.ubomb.view.ImageResource.DOOR_CLOSED;
import static fr.ubx.poo.ubomb.view.ImageResource.DOOR_OPENED;

public class SpriteDoor extends Sprite{

    public SpriteDoor(Pane layer, GameObject door) {
        super(layer, DOOR_CLOSED.getImage(), door);
        updateImage();
    }

    public void updateImage() {

        Doors door = (Doors) getGameObject();
        if(door.isDoorStatus()){
            setImage(DOOR_OPENED.getImage());
        }
    }

}
