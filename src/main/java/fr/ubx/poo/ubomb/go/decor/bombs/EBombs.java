package fr.ubx.poo.ubomb.go.decor.bombs;

import fr.ubx.poo.ubomb.game.Game;
import fr.ubx.poo.ubomb.game.Position;
import fr.ubx.poo.ubomb.go.GameObject;
import fr.ubx.poo.ubomb.go.character.Character;
import fr.ubx.poo.ubomb.go.decor.*;
import fr.ubx.poo.ubomb.go.decor.bonus.Bonus;
import fr.ubx.poo.ubomb.go.decor.bonus.Key;
import fr.ubx.poo.ubomb.go.decor.doors.Doors;

import java.util.ArrayList;
import java.util.List;

public class EBombs extends GameObject {

    private final int range;
    private final long startNow;
    int timer = 3;
    private boolean explodedB = false;

    public EBombs(Game game, Position position, long startNow) {
        super(game, position);
        this.startNow = startNow;
        this.range = game.getPlayer().getBombRange();
    }

    public int getTimer() {
        return timer;
    }

    public void setTimer(int timer) {
        this.timer = timer;
    }

    public void setExplodedB(boolean explodedB) {
        this.explodedB = explodedB;
    }

    public long getStartNow() {
        return startNow;
    }

    public List<Position> getExpPosition(long now) {
        List<Position> expPosition = new ArrayList<>();
        expPosition.add(this.getPosition());
        if (game.getPlayer().getPosition().equals(this.getPosition()) && !game.getPlayer().isInvincible()) {
            game.getPlayer().decHeart(now);
        }
        for (Character monster : game.getGrid().characters()) {
            if (monster.getPosition().equals(this.getPosition())) {
                monster.decHeart(now);
            }
        }
        for (int i = 1; i <= this.range; i++) {
            Position p = new Position(this.getPosition().getX(), this.getPosition().getY() + i);
            if (game.inside(p)) {
                Decor decor = game.getGrid().get(p);
                if (decor == null) {
                    expPosition.add(p);
                }
                if (decor instanceof Box || decor instanceof Bonus && !(decor instanceof Key)) {
                    decor.explode();
                    expPosition.add(p);
                    break;
                }
                if (decor instanceof Stone || decor instanceof Tree || decor instanceof Doors || decor instanceof Princess || decor instanceof Key) {
                    break;
                }
                if (game.getPlayer().getPosition().equals(p) && !game.getPlayer().isInvincible()) {
                    game.getPlayer().decHeart(now);
                }
                for (Character monster : game.getGrid().characters()) {
                    if (monster.getPosition().equals(p)) {
                        monster.decHeart(now);
                    }
                }
            }
        }
        for (int i = 1; i <= this.range; i++) {
            Position p = new Position(this.getPosition().getX(), this.getPosition().getY() - i);
            if (game.inside(p)) {
                Decor decor = game.getGrid().get(p);
                if (decor == null) {
                    expPosition.add(p);
                }
                if (decor instanceof Box || decor instanceof Bonus && !(decor instanceof Key)) {
                    decor.explode();
                    expPosition.add(p);
                    break;
                }
                if (decor instanceof Stone || decor instanceof Tree || decor instanceof Doors || decor instanceof Princess || decor instanceof Key) {
                    break;
                }
                if (game.getPlayer().getPosition().equals(p) && !game.getPlayer().isInvincible()) {
                    game.getPlayer().decHeart(now);
                }
                for (Character monster : game.getGrid().characters()) {
                    if (monster.getPosition().equals(p)) {
                        monster.decHeart(now);
                    }
                }
            }
        }

        for (int i = 1; i <= this.range; i++) {
            Position p = new Position(this.getPosition().getX() + i, this.getPosition().getY());
            if (game.inside(p)) {
                Decor decor = game.getGrid().get(p);
                if (decor == null) {
                    expPosition.add(p);
                }
                if (decor instanceof Box || decor instanceof Bonus && !(decor instanceof Key)) {
                    decor.explode();
                    expPosition.add(p);
                    break;
                }
                if (decor instanceof Stone || decor instanceof Tree || decor instanceof Doors || decor instanceof Princess || decor instanceof Key) {
                    break;
                }
                if (game.getPlayer().getPosition().equals(p) && !game.getPlayer().isInvincible()) {
                    game.getPlayer().decHeart(now);
                }
                for (Character monster : game.getGrid().characters()) {
                    if (monster.getPosition().equals(p)) {
                        monster.decHeart(now);
                    }
                }
            }
        }

        for (int i = 1; i <= this.range; i++) {
            Position p = new Position(this.getPosition().getX() - i, this.getPosition().getY());
            if (game.inside(p)) {
                Decor decor = game.getGrid().get(p);
                if (decor == null) {
                    expPosition.add(p);
                }
                if (decor instanceof Box || decor instanceof Bonus && !(decor instanceof Key)) {
                    decor.explode();
                    expPosition.add(p);
                    break;
                }
                if (decor instanceof Stone || decor instanceof Tree || decor instanceof Doors || decor instanceof Princess || decor instanceof Key) {
                    break;
                }
                if (game.getPlayer().getPosition().equals(p) && !game.getPlayer().isInvincible()) {
                    game.getPlayer().decHeart(now);
                }
                for (Character monster : game.getGrid().characters()) {
                    if (monster.getPosition().equals(p)) {
                        monster.decHeart(now);
                    }
                }
            }
        }
        return expPosition;
    }

    @Override
    public boolean isWalkable(Character player) {
        return false;
    }


    public boolean isExploded() {
        return explodedB;
    }
}
