package fr.ubx.poo.ubomb.go.decor.doors;

import fr.ubx.poo.ubomb.game.Game;
import fr.ubx.poo.ubomb.game.Position;

public class DoorPrevOpened extends Doors {

    public DoorPrevOpened(Game game, Position position) {
        super(game,position,true,false);
    }
}
