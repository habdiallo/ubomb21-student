package fr.ubx.poo.ubomb.go.decor.doors;

import fr.ubx.poo.ubomb.game.Game;
import fr.ubx.poo.ubomb.game.Position;

public class DoorNextClosed extends Doors {

    public DoorNextClosed(Game game, Position position) {
        super(game, position,false,true);
    }
}