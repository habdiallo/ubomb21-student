package fr.ubx.poo.ubomb.go.decor.doors;

import fr.ubx.poo.ubomb.game.Game;
import fr.ubx.poo.ubomb.game.Position;
import fr.ubx.poo.ubomb.go.Takeable;
import fr.ubx.poo.ubomb.go.character.Character;
import fr.ubx.poo.ubomb.go.character.Player;
import fr.ubx.poo.ubomb.go.decor.Decor;

public class Doors extends Decor implements Takeable {


    private final boolean doorPossibleTogoNext;
    private boolean doorStatus ;

    //constructor
    public Doors(Game game, Position position, boolean doorStatus, boolean doorPossibleTogoNext) {
        super(game, position);
        this.doorStatus = doorStatus;
        this.doorPossibleTogoNext = doorPossibleTogoNext;

    }

    public boolean isDoorStatus() {
        return doorStatus;
    }

    public void setDoorStatus() {
        this.doorStatus = true;
    }

    public boolean isDoorPossibleTogoNext() {
        return doorPossibleTogoNext;
    }

    @Override
    public void takenBy(Player player) {
        player.takeDoor();
    }


    @Override
    public boolean isWalkable(Character player) {
        return doorStatus;
    }



}
