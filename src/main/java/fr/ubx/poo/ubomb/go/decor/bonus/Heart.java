package fr.ubx.poo.ubomb.go.decor.bonus;

import fr.ubx.poo.ubomb.game.Position;
import fr.ubx.poo.ubomb.go.character.Character;
import fr.ubx.poo.ubomb.go.character.Player;

public class Heart extends Bonus {
    public Heart (Position position) {
        super(position);
    }

    @Override
    public boolean isWalkable(Character player) {
        return true;
    }


    public void takenBy(Player player) {player.takeHeart();}
}