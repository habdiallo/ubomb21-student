package fr.ubx.poo.ubomb.go.character;

import fr.ubx.poo.ubomb.game.Direction;
import fr.ubx.poo.ubomb.game.Game;
import fr.ubx.poo.ubomb.game.Position;
import fr.ubx.poo.ubomb.go.decor.Decor;

public class Monster extends Character{

    @Override
    public int getLives() {
        return lives;
    }

    private int lives = 3;

    private int timeToMove = 0;

    public Monster(Game game, Position position, int lives) {
        super(game, position, lives);
    }

    public void setTimeToMove(int timeToMove) {
        this.timeToMove = timeToMove;
    }


    @Override
    public void update(long now) {
        {
            int secondMove = (int) (now / 1e9);
            if (secondMove - timeToMove > 0.01){
                this.requestMove(Direction.random());
                super.update(now);
                setTimeToMove(secondMove);
            }
        }
    }

    @Override
    public boolean canMove(Direction direction) {
        Position Pos = direction.nextPosition(this.getPosition());
        Decor dec = this.game.getGrid().get(Pos);

        if (!this.game.inside(Pos)){
            return false;
        }
        return dec == null;
    }

    @Override
    public void decHeart(long now) {
        lives--;
        super.invincible = true;
        super.startInvincibleNow = now;
        if (lives==0){
            this.remove();
        }
    }

    @Override
    public void doMove(Direction direction) {
        game.getGrid().removeCharacter(getPosition());
        Position nextPos = direction.nextPosition(getPosition());
        game.getGrid().setCharacterMap(nextPos, this);
        setPosition(nextPos);
    }

}
