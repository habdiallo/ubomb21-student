package fr.ubx.poo.ubomb.go.character;

import fr.ubx.poo.ubomb.game.Direction;
import fr.ubx.poo.ubomb.game.Game;
import fr.ubx.poo.ubomb.game.Position;
import fr.ubx.poo.ubomb.go.GameObject;
import fr.ubx.poo.ubomb.go.Movable;
import fr.ubx.poo.ubomb.go.decor.Box;
import fr.ubx.poo.ubomb.go.decor.Decor;


/*Cette classe traitera tous ce qui est entre Monster et Player
 *
 */
public class Character extends GameObject implements Movable {

    private final int lives;
    private Direction direction;
    private boolean moveRequested = false;

    // invisibility & collision
    boolean invincible = false;
    public boolean isInvincible() {
        return invincible;
    }

    public void setInvincible(boolean invincible) {
        this.invincible = invincible;
    }

    long startInvincibleNow;


    public Character(Game game, Position position, int lives) {
        super(game, position);
        this.direction = Direction.DOWN;
        this.lives = lives;
    }

    public Direction getDirection() {
        return direction;
    }
    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public int getLives() {
        return lives;
    }


    public void checkInvincible(long now){
        int secondMinuter = (int) ((now - startInvincibleNow)/1e9)%10;
        if (secondMinuter > 1){
            setInvincible(false);
            startInvincibleNow = 0;
        }
    }

    public void requestMove(Direction direction) {
        if (direction != this.direction) {
            this.direction = direction;
            setModified(true);
        }
        moveRequested = true;
    }

    @Override
    public boolean canMove(Direction direction) {
        Position Pos = direction.nextPosition(this.getPosition());
        Decor dec = this.game.getGrid().get(Pos);

        if (!this.game.inside(Pos)){
            return false;
        }
        if ( dec instanceof Box && this.game.getGrid().get(direction.nextPosition(Pos))==null
                && this.game.getGrid().getCharacter(direction.nextPosition(Pos))==null && this.game.inside(direction.nextPosition(Pos))){
            return true;
        }
        return dec == null || dec.isWalkable(this);
    }

    public void decHeart(long now) {
    }

    public void update(long now) {
        if (moveRequested) {
            if (canMove(direction)) {
                doMove(direction);
            }
        }
        moveRequested = false;
    }
    
    @Override
    public void doMove(Direction direction) {
    }

    @Override
    public boolean isWalkable(Character player) {
        return false;
    }

}
