/*
 * Copyright (c) 2020. Laurent Réveillère
 */

package fr.ubx.poo.ubomb.go.character;

import fr.ubx.poo.ubomb.game.Direction;
import fr.ubx.poo.ubomb.game.Game;
import fr.ubx.poo.ubomb.game.Position;
import fr.ubx.poo.ubomb.go.GameObject;
import fr.ubx.poo.ubomb.go.decor.Box;
import fr.ubx.poo.ubomb.go.decor.Decor;
import fr.ubx.poo.ubomb.go.decor.Princess;
import fr.ubx.poo.ubomb.go.decor.bonus.Bonus;
import fr.ubx.poo.ubomb.go.decor.doors.DoorNextClosed;
import fr.ubx.poo.ubomb.go.decor.doors.Doors;


public class Player extends Character {

    // Vie et Clé
    private int lives;
    private int keys;
    // Bomb
    private int bombRange = 1;
    private int bombNumber;
    private boolean winner;

    public Player(Game game, Position position, int lives) {
        super(game, position, lives);
        this.lives = lives;
    }

    @Override
    public int getLives() {
        return lives;
    }

    public int getKeys() {
        return keys;
    }

    public int getBombRange() {
        return bombRange;
    }

    public int getBombNumber() {
        return bombNumber;
    }

    public void doMove(Direction direction) {
        // Check if we need to pick something up
        Position nextPos = direction.nextPosition(getPosition());
        setPosition(nextPos);
        Position Poss = direction.nextPosition(nextPos);
        Decor dec = this.game.getGrid().get(nextPos);

        GameObject iciDeco = this.game.getGrid().get(Poss);

        if (dec instanceof Box && iciDeco == null && this.game.inside(Poss)) {
            ((Box) dec).doMove(direction);
            this.game.getGrid().set(Poss, dec);
            this.game.getGrid().remove(nextPos);
        }
        if (dec instanceof Bonus) {
            ((Bonus) dec).takenBy(game.getPlayer());
            dec.remove();
        }
        decorManaging(dec);
    }

    @Override
    public void explode() {
    }

    // Example of methods to define by the player
    public void takeDoor() {
        doorOpening();
    }

    public void doorOpening() {
        Position pos = getDirection().nextPosition(getPosition());
        Decor dec = this.game.getGrid().get(pos);
        if (dec instanceof DoorNextClosed && getKeys() > 0) {
            ((DoorNextClosed) dec).setDoorStatus();
            dec.setModified(true);
            keyUsed();
        }
    }

    public void takeHeart() {
        lives++;
    }

    @Override
    public void decHeart(long now) {
        lives--;
        super.invincible = true;
        super.startInvincibleNow = now;
    }

    public void takeKey() {
        keys++;
    }

    public void keyUsed() {
        keys--;
    }

    public void incBombRange() {
        bombRange++;
    }

    public void decBombRange() {
        bombRange--;
    }

    public void incBombNumber() {
        bombNumber++;
    }

    public void decBombNumber() {
        bombNumber--;
    }

    public boolean isWinner() {
        return winner;
    }

    public void setWinner(boolean winner) {
        this.winner = winner;
    }


    //Gestion des decors

    /**
     * Ici j'ai definie les comportement du joueur en cas de contact avec certain decor
     * par exemple la princesse ou les portes
     */
    private void decorManaging(Decor decor) {
        if (decor instanceof Princess) {
            setWinner(true);
        }
        if (decor instanceof Doors) {
            doorManaging(decor);
        }
    }

    /**
     * Comportement du joueur sur les different porte des trois niveau
     */
    public void doorManaging(Decor decor) {
        if (decor instanceof Doors && ((Doors) decor).isDoorPossibleTogoNext()) {
            game.goToNextLevel();
            game.setLevelChanged(true);

        } else {
            game.gotoPrevLevel();
            game.setLevelChanged(true);
        }
    }
}
