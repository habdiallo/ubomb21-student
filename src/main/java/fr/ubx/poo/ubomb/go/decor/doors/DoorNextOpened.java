package fr.ubx.poo.ubomb.go.decor.doors;

import fr.ubx.poo.ubomb.game.Game;
import fr.ubx.poo.ubomb.game.Position;

public class DoorNextOpened extends Doors {
    public DoorNextOpened(Game game, Position position) {
        super(game, position,true,true);
    }
}
