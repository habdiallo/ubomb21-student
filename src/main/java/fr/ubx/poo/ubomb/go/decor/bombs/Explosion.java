package fr.ubx.poo.ubomb.go.decor.bombs;

import fr.ubx.poo.ubomb.game.Game;
import fr.ubx.poo.ubomb.game.Position;
import fr.ubx.poo.ubomb.go.GameObject;
import fr.ubx.poo.ubomb.go.character.Character;

public class Explosion extends GameObject {



    public Explosion(Game game, Position position, long explodedTime) {
        super(game, position);
    }

    @Override
    public boolean isWalkable(Character player) {
        return true;
    }

    @Override
    public void explode() {
    }
}
