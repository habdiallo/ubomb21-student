package fr.ubx.poo.ubomb.game;

import fr.ubx.poo.ubomb.go.character.Character;
import fr.ubx.poo.ubomb.go.decor.Decor;

import java.util.*;

public class Grid {


    private final int width;
    private final int height;
    private final Map<Position, Decor> elements;
    private final Map<Position, Character> characterMap;

    public Grid(int width, int height) {
        this.width = width;
        this.height = height;
        this.elements = new Hashtable<>();
        this.characterMap = new Hashtable<>();
    }

    public List<Character> getMonsterGrid() {
        return new ArrayList<>(characters());
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public Decor get(Position position) {
        return elements.get(position);
    }

    public void set(Position position, Decor decor) {
        if (decor != null)
            elements.put(position, decor);
    }

    public void remove(Position position) {
        elements.remove(position);
    }

    public Collection<Decor> values() {
        return elements.values();
    }


    public Character getCharacter(Position position) {
        return characterMap.get(position);
    }

    public void setCharacterMap(Position position, Character character) {
        if (character != null)
            characterMap.put(position, character);
    }

    public void removeCharacter(Position position) {
        characterMap.remove(position);
    }

    public Collection<Character> characters() {
        return characterMap.values();
    }

}