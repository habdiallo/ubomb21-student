package fr.ubx.poo.ubomb.game;

import java.io.*;

public class GridRepoFile extends GridRepo {
    GridRepoFile(Game game) {
        super(game);
    }

    @Override
    public Grid load(int level, String name) {
        BufferedReader br = null;
        try {
            FileReader fileReader = new FileReader("src/main/resources/sample/" + name + level+".txt");
            br = new BufferedReader(fileReader);
            StringBuilder sb = new StringBuilder();
            String line;
            int i;
            int countLine = 0;
            int countColumn = 0;
            char charLine;
            while ((line = br.readLine()) != null) {
                sb.append(line).append("\n");
            }
            fileReader.close();

            String gString = sb.toString();

            if("".compareTo(gString) == 0){
                throw new GridException("the character string is empty");
            }
            if(gString.charAt(0)=='\n'){
                throw new GridException("row size is zero");
            }

            for (i = 0; i < gString.length(); i++){
                charLine= gString.charAt(i);
                if (charLine == '\n'){
                    if (countLine==0){
                        countColumn = i;
                    }
                    countLine++;
                }

            }
            int height = countLine;
            int width = countColumn;
            Grid grid = new Grid(width, height);
            i = 0;
            for (int j = 0; j < height; j++) {
                for (int k = 0; k < width; k++) {
                    charLine = gString.charAt(i);
                    if (charLine != '\n') {
                        Position position = new Position(k, j);
                        EntityCode entityCode = EntityCode.fromCode(gString.charAt(i));
                        grid.set(position, processEntityCode(entityCode, position));
                        grid.setCharacterMap(position, processCharacters(entityCode, position));
                        i++;
                    }
                }i++;

            }
            return grid;
        } catch (IOException e) {
            e.printStackTrace();
        }
        catch (NullPointerException nullPointerException) {
            System.err.println("file not opened" + (br != null ? br.toString() : null));
        }

    return null;}
}
