package fr.ubx.poo.ubomb.game;

import fr.ubx.poo.ubomb.go.character.Character;
import fr.ubx.poo.ubomb.go.character.Monster;
import fr.ubx.poo.ubomb.go.decor.*;
import fr.ubx.poo.ubomb.go.decor.bonus.BombNumberDec;
import fr.ubx.poo.ubomb.go.decor.bonus.BombNumberInc;
import fr.ubx.poo.ubomb.go.decor.bonus.BombRangeDec;
import fr.ubx.poo.ubomb.go.decor.bonus.BombRangeInc;
import fr.ubx.poo.ubomb.go.decor.bonus.Heart;
import fr.ubx.poo.ubomb.go.decor.bonus.Key;
import fr.ubx.poo.ubomb.go.decor.doors.DoorNextClosed;
import fr.ubx.poo.ubomb.go.decor.doors.DoorNextOpened;
import fr.ubx.poo.ubomb.go.decor.doors.DoorPrevOpened;

import java.io.IOException;


public abstract class GridRepo {

    private final Game game;

    GridRepo(Game game) {
        this.game = game;
    }

    public Game getGame() {
        return game;
    }

    public abstract Grid load(int level, String name) throws IOException;

    Decor processEntityCode(EntityCode entityCode, Position pos) {
        switch (entityCode) {
            case Stone:
                return new Stone(pos);
            case Tree:
                return new Tree(pos);
            case Key:
                return new Key(pos);
            case Box:
                return new Box(pos);
            case Princess:
                return new Princess(pos);
            case DoorPrevOpened:
                return new DoorPrevOpened(game, pos);
            case DoorNextOpened:
                return new DoorNextOpened(game, pos);
            case DoorNextClosed:
                return new DoorNextClosed(game, pos);
            case Heart:
                return new Heart(pos);
            case BombRangeInc:
                return new BombRangeInc(pos);
            case BombRangeDec:
                return new BombRangeDec(pos);
            case BombNumberInc:
                return new BombNumberInc(pos);
            case BombNumberDec:
                return new BombNumberDec(pos);

            default:
                return null;
            // throw new RuntimeException("EntityCode " + entityCode.name() + " not processed");
        }
    }

    Character processCharacters(EntityCode entityCode, Position pos) {
        if (entityCode == EntityCode.Monster) {
            return new Monster(this.game, pos, 3);
        }
        return null;
        //throw new IllegalStateException("Unexpected value: " + entityCode);
    }


}
