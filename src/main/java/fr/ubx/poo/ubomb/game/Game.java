/*
 * Copyright (c) 2020. Laurent Réveillère
 */

package fr.ubx.poo.ubomb.game;


import fr.ubx.poo.ubomb.go.character.Player;
import fr.ubx.poo.ubomb.go.decor.Decor;
import fr.ubx.poo.ubomb.go.decor.bombs.EBombs;
import fr.ubx.poo.ubomb.go.decor.doors.Doors;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class Game {

    public final int bombBagCapacity;
    public final int bombRange;
    public final int gKeys;
    public final int monsterVelocity;
    public final int playerLives;
    public final int levels;
    public final long playerInvisibilityTime;
    public final long monsterInvisibilityTime;

    private final Player player;
    //Level storage list
    private final List<Grid> gridLevel = new ArrayList<>();
    //grid used for changing level
    private Grid grid;
    //Bomb storage list
    public List<EBombs> bombList = new ArrayList<>();

    //return true or false when level was changed
    private boolean levelChanged = false;
    //Our current level
    private int currentGameLevel;
    //level where player am from
    private int fromLevel;

    public Game(String worldPath) {
        try (InputStream input = new FileInputStream(new File(worldPath, "config.properties"))) {
            Properties prop = new Properties();
            // load the configuration file
            prop.load(input);
            bombBagCapacity = Integer.parseInt(prop.getProperty("bombBagCapacity", "3"));
            bombRange = Integer.parseInt(prop.getProperty("bombRange", "1"));
            gKeys = Integer.parseInt(prop.getProperty("gKeys", "0"));
            monsterVelocity = Integer.parseInt(prop.getProperty("monsterVelocity", "10"));
            levels = Integer.parseInt(prop.getProperty("levels", "1"));
            playerLives = Integer.parseInt(prop.getProperty("playerLives", "3"));
            playerInvisibilityTime = Long.parseLong(prop.getProperty("playerInvisibilityTime", "4000"));
            monsterInvisibilityTime = Long.parseLong(prop.getProperty("monsterInvisibilityTime", "1000"));

            // Load the world
            String prefix = prop.getProperty("prefix");
            GridRepo gridRepoF = new GridRepoFile(this);

            //level list adding
            for (int i = 1; i <= levels; i++) {
                gridLevel.add(gridRepoF.load(i, prefix));
            }
            this.grid = gridLevel.get(0);


            // Create the player
            String[] tokens = prop.getProperty("player").split("[ :x]+");
            if (tokens.length != 2)
                throw new RuntimeException("Invalid configuration format");
            Position playerPosition = new Position(Integer.parseInt(tokens[0]), Integer.parseInt(tokens[1]));
            player = new Player(this, playerPosition, playerLives);

        } catch (IOException ex) {
            System.err.println("Error loading configuration");
            throw new RuntimeException("Invalid configuration format");
        }
    }

    public List<Grid> getGridLevel() {
        return gridLevel;
    }

    public boolean isLevelChanged() {
        return levelChanged;
    }

    public void setLevelChanged(boolean levelChanged) {
        this.levelChanged = levelChanged;
    }

    public int getCurrentGameLevel() {
        return currentGameLevel;
    }

    public Grid getGrid() {
        return grid;
    }

    public void setGrid(Grid grid) {
        this.grid = grid;
    }

    public Player getPlayer() {
        return this.player;
    }


    //grid inside
    public boolean inside(Position position) {
        if (position.getY() > this.getGrid().getHeight() - 1 ||
                position.getY() < 0) {
            return false;
        }
        return position.getX() <= this.getGrid().getWidth() - 1 &&
                position.getX() >= 0;
    }


    //toggle between levels
    public void goToNextLevel() {
        fromLevel = currentGameLevel;
        currentGameLevel++;
        this.setLevelChanged(true);
    }

    public void gotoPrevLevel() {
        fromLevel = currentGameLevel;
        currentGameLevel--;
        this.setLevelChanged(true);
    }

    //looking for door position

    /**
     * Localisation de la position de la porte d'entré ou de sortie
     * pour definir la prochaine position sur la porte indiqué
     */
    public Position doorIsThere() {
        for (Decor decor : getGrid().values()) {
            if (currentGameLevel > fromLevel) {
                if (decor instanceof Doors && !((Doors) decor).isDoorPossibleTogoNext()) {
                    return decor.getPosition();
                }
            } else {
                if (decor instanceof Doors && ((Doors) decor).isDoorPossibleTogoNext()) {
                    return decor.getPosition();
                }
            }
        }
        return null;
    }
}
